﻿using System;

namespace Domain.Commands
{
    public class CommandInvoker : ICommandInvoker
    {
        private readonly IServiceProvider _serviceProvider;

        public CommandInvoker(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public TResult Invoke<TResult>(ICommand<TResult> command) where TResult : ICommandResult
        {
            return Invoke<ICommand<TResult>, TResult>(command);
        }

        private TResult Invoke<T, TResult>(T command) where T : ICommand<TResult> where TResult : ICommandResult
        {
            return ResolveHandlerAndInvokeCommand(command);
        }

        private TResult ResolveHandlerAndInvokeCommand<TResult>(ICommand<TResult> command) where TResult : ICommandResult
        {
            var commandHandlerGenericType = typeof(ICommandHandler<,>);
            var commandHandlerType = commandHandlerGenericType.MakeGenericType(command.GetType(), typeof(TResult));
            var handleMethod = commandHandlerType.GetMethod(nameof(ICommandHandler<ICommand<TResult>, TResult>.Handle));

            var commandHandler = _serviceProvider.GetService(commandHandlerType);
            if (commandHandler == null)
            {
                throw new InvalidOperationException($"Could not resolve CommandHandler for Command: {command.GetType()}. Ensure that CommandHandler is registered.");
            }

            return (TResult)handleMethod.Invoke(commandHandler, new object[] { command });
        }
    }
}