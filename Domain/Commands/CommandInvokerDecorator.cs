﻿namespace Domain.Commands
{
    public abstract class CommandInvokerDecorator : ICommandInvoker
    {
        public ICommandInvoker WrappedCommandInvoker { get; private set; }

        protected CommandInvokerDecorator(ICommandInvoker wrappedCommandInvoker)
        {
            WrappedCommandInvoker = wrappedCommandInvoker;
        }

        public abstract TResult Invoke<TResult>(ICommand<TResult> command) where TResult : ICommandResult;
    }
}