﻿using System.Transactions;
using Domain.Utilities;

namespace Domain.Commands
{
    public class CommandInvokerTransactionDecorator : CommandInvokerDecorator
    {
        private readonly ITransactionScopeFactory _txScopeFactory;

        public CommandInvokerTransactionDecorator(ICommandInvoker innerCommandInvoker, ITransactionScopeFactory txScopeFactory) : base(innerCommandInvoker)
        {
            _txScopeFactory = txScopeFactory;
        }

        public override TResult Invoke<TResult>(ICommand<TResult> command) 
        {
            if (Transaction.Current != null)
            {
                return WrappedCommandInvoker.Invoke(command);
            }
            using (var txScope = _txScopeFactory.Create())
            {
                var result = WrappedCommandInvoker.Invoke(command);
                if (result.IsSuccess)
                {
                    txScope.Complete();
                }
                return result;
            }
        }
    }
}
