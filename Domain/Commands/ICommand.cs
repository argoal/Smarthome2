﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Commands
{
    public interface ICommand
    {
    }

    public interface ICommand<out T> : ICommand where T : ICommandResult
    {
    }

    public interface ICommandResult
    {
        IEnumerable<string> ErrorMessages { get; }
        bool IsSuccess { get; }
        bool IsFailure { get; }
    }

    public class CommandResult : ICommandResult
    {
        public IEnumerable<string> ErrorMessages { get; protected set; }
        public bool IsSuccess => !ErrorMessages.Any();
        public bool IsFailure => !IsSuccess;

        public CommandResult(params string[] errors)
        {
            this.ErrorMessages = errors;
        }

        public static CommandResult Success => new CommandResult();
        public static CommandResult Failure(params string[] errors) => new CommandResult(errors);
    }
}
