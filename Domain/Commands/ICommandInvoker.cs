﻿namespace Domain.Commands
{
    public interface ICommandInvoker
    {
        TResult Invoke<TResult>(ICommand<TResult> command) where TResult : ICommandResult;
    }
}
