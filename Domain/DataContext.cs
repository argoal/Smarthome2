﻿using System.Linq;
using Domain.Entities;
using Domain.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    //TODO: DataContext oli enne muutmist SmarthomeContext 

    public class DataContext : DbContext     //see klass ja pärinemine loovad ligipääsu EF funktsionaalsusele, see klass tuleb registreerida service'ina startup.cs faili
    {
        //Options antakse kaasa baasklassile
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TemperatureReading>(eb =>
            {
                eb.Property(b => b.Sauna).HasColumnType("NUMERIC");
                eb.Property(b => b.Boiler).HasColumnType("NUMERIC");
                eb.Property(b => b.Outside).HasColumnType("NUMERIC");
                eb.Property(b => b.Basement).HasColumnType("NUMERIC");
                eb.Property(b => b.FirstFloor).HasColumnType("NUMERIC");
                eb.Property(b => b.SecondFloor).HasColumnType("NUMERIC");
            });
        }
        

        public DbSet<TemperatureReading> TemperatureReadings { get; set; }  // iga rida siin on üks andmebaasi tabel, tabeli nimi on selle property nimi, nimed on tavaliselt mitmuses
        public DbSet<HumidityReading> HumidityReadings { get; set; }

        
        public virtual IQueryable<T> GetQueryBy<T>(Specification<T> specification) where T : class
        {
            return specification.SatisfyEntitiesFrom(Set<T>());
        }
    }
}
