﻿using System;

namespace Domain.Entities
{
    public class HumidityReading
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int Basement { get; set; }
        public int SecondFloor { get; set; }
    }
}
