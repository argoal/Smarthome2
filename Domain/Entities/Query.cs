﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities
{
    public interface IQuery<T> where T : class
    {
        IQueryable<T> Get(Specification<T> spec);
        T FindById(object id);
    }
    public class Query<T> : IQuery<T> where T : class
    {
        private readonly DataContext _context;

        public Query(DataContext context)
        {
            _context = context;
        }

        public IQueryable<T> Get(Specification<T> spec)
        {
            return _context.GetQueryBy(spec).AsNoTracking();
        }

        public T FindById(object id)
        {
            var entity = _context.Set<T>().Find(id);
            if (entity != null)
            {
                _context.Entry(entity).State = EntityState.Detached;
            }
            return entity;
        }
    }
}
