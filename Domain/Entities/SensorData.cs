﻿namespace Domain.Entities
{
    public class SensorData
    {        
        public decimal? BasementHumidity { get; set; }
        public decimal? SecondFloorHumidity { get; set; }
        public decimal? BasementTemperature { get; set; }
        public decimal? FirstFloorTemperature { get; set; }
        public decimal? SecondFloorTemperature { get; set; }
        public decimal? OutsideTemperature { get; set; }
        public decimal? HeatingTemperature { get; set; }
        public decimal? SaunaTemperature { get; set; }


    } 
}
