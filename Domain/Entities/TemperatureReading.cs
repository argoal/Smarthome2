﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class TemperatureReading
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
//        [Column(TypeName = "NUMERIC")] need saab ka seadistada data context klassis
        public decimal Basement { get; set; }
        public decimal FirstFloor { get; set; }
        public decimal SecondFloor { get; set; }
        public decimal Sauna { get; set; }
        public decimal Boiler { get; set; }
        public decimal Outside { get; set; }

        public TemperatureReading()
        {

        }

        //TODO
        public TemperatureReading(DateTime timestamp, decimal basement, decimal firstFloor, decimal secondFloor, decimal sauna, decimal boiler, decimal outside)
        {
            Timestamp = timestamp;
            Basement = basement;
            FirstFloor = firstFloor;
            SecondFloor = secondFloor;
            Sauna = sauna;
            Boiler = boiler;
            Outside = outside;
        }
        public TemperatureReading(int id, DateTime timestamp, decimal basement, decimal firstFloor, decimal secondFloor, decimal sauna, decimal boiler, decimal outside)
        {
            Id = id;
            Timestamp = timestamp;
            Basement = basement;
            FirstFloor = firstFloor;
            SecondFloor = secondFloor;
            Sauna = sauna;
            Boiler = boiler;
            Outside = outside;
        }


    }
}
