﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace Domain.Specifications
{
    public abstract class SortSpecification<TEntity> : ISortSpecification<TEntity>, IEquatable<SortSpecification<TEntity>>
    {
        private readonly List<SortSpecification<TEntity>> _thenBySpecifications = new List<SortSpecification<TEntity>>();

        protected SortSpecification(bool descending)
        {
            Descending = descending;
        }

        private bool Descending { get; }

        public IQueryable<TEntity> Sort(IQueryable<TEntity> query)
        {
            var ordered = Descending ? query.OrderByDescending(KeySelector) : query.OrderBy(KeySelector);
            foreach (var thenBy in _thenBySpecifications)
            {
                ordered = thenBy.Descending ? ordered.ThenByDescending(thenBy.KeySelector) : ordered.ThenBy(thenBy.KeySelector);
            }
            return ordered;
        }

        public SortSpecification<TEntity> ThenBy(SortSpecification<TEntity> thenBy)
        {
            _thenBySpecifications.Add(thenBy);
            return this;
        }

        internal abstract Expression<Func<TEntity, object>> KeySelector { get; }

        private bool AreThenBySpecsEqual(SortSpecification<TEntity> other)
        {
            if (_thenBySpecifications.Count() != other._thenBySpecifications.Count())
            {
                return false;
            }

            for (var i = 0; i < _thenBySpecifications.Count(); i++)
            {
                if (!Equals(_thenBySpecifications[i], other._thenBySpecifications[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public bool Equals(SortSpecification<TEntity> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return AreThenBySpecsEqual(other) && Descending == other.Descending;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SortSpecification<TEntity>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_thenBySpecifications != null ? _thenBySpecifications.GetHashCode() : 0) * 397) ^ Descending.GetHashCode();
            }
        }
    }

    /// <summary>
    /// Do not use this interface directly for implementing specifications, use abstract SortSpecification<TEntity, Tkey> class for that.
    /// </summary>
    public interface ISortSpecification<TEntity>
    {
        IQueryable<TEntity> Sort(IQueryable<TEntity> query);
    }
}