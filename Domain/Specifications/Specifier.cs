﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Specifications
{
    /// <summary>
    /// </summary>
    public interface ISpecifier
    {
        bool IsEntitySatisfiedBy<T>(T entity, ISpecification<T> specification);
        IQueryable<T> SatisfyEntitiesFrom<T>(IQueryable<T> entityQuery, ISpecification<T> specification);
        IEnumerable<T> SatisfyEntitiesFrom<T>(IEnumerable<T> entityQuery, ISpecification<T> specification);
    }

    public class Specifier : ISpecifier
    {
        public bool IsEntitySatisfiedBy<T>(T entity, ISpecification<T> specification)
        {
            return specification.IsSatisfiedBy(entity);
        }

        public IQueryable<T> SatisfyEntitiesFrom<T>(IQueryable<T> entityQuery, ISpecification<T> specification)
        {
            return specification.SatisfyEntitiesFrom(entityQuery);
        }

        public IEnumerable<T> SatisfyEntitiesFrom<T>(IEnumerable<T> entityQuery, ISpecification<T> specification)
        {
            return specification.SatisfyEntitiesFrom(entityQuery.AsQueryable());
        }
    }
}
