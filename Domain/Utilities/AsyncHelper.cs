﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Domain.Utilities
{
    /// <summary>
    /// Use this helper to run async code synchronously avoiding deadlocks (http://stackoverflow.com/questions/15021304/an-async-await-example-that-causes-a-deadlock).
    /// This code is stolen from Microsoft.AspNet.Identity.Core Microsoft.AspNet.Identity.AsyncHelper internal class
    /// Warning! Does not participate in ambient transactions if async flow is not enabled
    /// </summary>
    public static class AsyncHelper
    {
        private static readonly TaskFactory MyTaskFactory = new
          TaskFactory(CancellationToken.None,
                      TaskCreationOptions.None,
                      TaskContinuationOptions.None,
                      TaskScheduler.Default);

        /// <summary>
        /// Warning! Does not participate in ambient transactions if async flow is not enabled
        /// </summary>
        public static TResult RunSync<TResult>(Func<Task<TResult>> func)
        {
            return MyTaskFactory
                .StartNew(func)
              .Unwrap<TResult>()
              .GetAwaiter()
              .GetResult();
        }

        /// <summary>
        /// Warning! Does not participate in ambient transactions if async flow is not enabled
        /// </summary>
        public static void RunSync(Func<Task> func)
        {
            MyTaskFactory
              .StartNew(func)
              .Unwrap()
              .GetAwaiter()
              .GetResult();
        }
    }
}
