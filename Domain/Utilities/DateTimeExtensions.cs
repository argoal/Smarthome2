﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Utilities
{
    public static class DateTimeExtensions
    {
        public static string ToDoubleDigitFormat(this DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }
    }
}
