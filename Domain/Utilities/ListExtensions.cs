﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Utilities
{
    public static class ListExtensions
    {
        public static T Pop<T>(this List<T> list)
        {
            var last = list.Last();
            list.Remove(last);
            return last;
        }

        public static T PopFirst<T>(this List<T> list)
        {
            var first = list.First();
            list.Remove(first);
            return first;
        }
    }
}
