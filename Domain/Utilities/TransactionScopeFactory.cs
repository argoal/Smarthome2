﻿using System;
using System.Transactions;

namespace Domain.Utilities
{
    public interface ITransactionScopeFactory
    {
        ITransactionScope Create();
    }

    public class TransactionScopeFactory : ITransactionScopeFactory
    {
        public ITransactionScope Create()
        {
            return new TransactionScopeWrapper(IsolationLevel.ReadCommitted, TransactionScopeOption.Required, TimeSpan.FromSeconds(30));
        }

        private class TransactionScopeWrapper : ITransactionScope
        {
            private readonly TransactionScope _transactionScope;

            public TransactionScopeWrapper(IsolationLevel isolationLevel, TransactionScopeOption txOption, TimeSpan timeout)
            {
                _transactionScope = new TransactionScope(txOption, new TransactionOptions { IsolationLevel = isolationLevel, Timeout = timeout }, TransactionScopeAsyncFlowOption.Enabled);
            }

            public void Complete()
            {
                _transactionScope.Complete();
            }

            public void Dispose()
            {
                _transactionScope.Dispose();
            }
        }
    }
    public interface ITransactionScope : IDisposable
    {
        void Complete();
    }
}