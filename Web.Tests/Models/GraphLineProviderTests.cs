﻿using Domain.Entities;
using Domain.Specifications;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Web.Models;

namespace Web.Tests.Models
{
    [TestFixture]
    public class GraphLineProviderTests
    {
        private Mock<IQuery<TemperatureReading>> _temperatureReadingQueryMock;
        private Mock<IConfiguration> _configurationMock;
        private GraphLine.Provider _sut;

        [SetUp]
        public void SetUp()
        {
            _temperatureReadingQueryMock = new Mock<IQuery<TemperatureReading>>();
            _configurationMock = new Mock<IConfiguration>();
            _sut = new GraphLine.Provider(_temperatureReadingQueryMock.Object, _configurationMock.Object);


        }

        [Test]
       public void Provide_Does_Not_Return_Older_Graphlines_Than_Described_With_Filter()
       {
            var temperatureReading1 = new TemperatureReading()
            {
                Id = 1,
                Timestamp = DateTime.Now.AddDays(-2),
                Basement = 21,
                FirstFloor = 22,
                SecondFloor = 23,
                Outside = 24,
                Sauna = 25,
                Boiler = 26
            };

            var temperatureReading2 = new TemperatureReading()
            {
                Id = 2,
                Timestamp = DateTime.Now.AddHours(-12),
                Basement = 31,
                FirstFloor = 32,
                SecondFloor = 33,
                Outside = 34,
                Sauna = 35,
                Boiler = 36
            };

            //ei tohiks valida neid TemperatureReading'uid mille vanus on üle 1 päeva
            var graphFilters = new GraphFilters(1, "Basement");

            _configurationMock.Setup(x => x.GetValue<int>("UserVariables:MinValueOnGraph")).Returns(-40);
            
            _temperatureReadingQueryMock.Setup(q => q.Get(new MatchAllSpecification<TemperatureReading>())).Returns(new[] { temperatureReading1, temperatureReading2 }.AsQueryable());

            //Act
            var result = _sut.Provide(graphFilters);

            //Assert

            Assert.That(result.Count<GraphLine>, Is.EqualTo(1));
            Assert.That(DateTime.Parse(result[0].Date), Is.InRange(DateTime.Now.AddHours(-12).AddMinutes(-2), DateTime.Now.AddHours(-12).AddMinutes(2)));
       }

        [Test]
        public void Provide_Returns_All_Temperature_Values_When_Filter_Symbol_Is_Null()
        {
            var temperatureReading1 = new TemperatureReading()
            {
                Id = 1,
                Timestamp = DateTime.Now.AddDays(-2),
                Basement = 21,
                FirstFloor = 22,
                SecondFloor = 23,
                Outside = 24,
                Sauna = 25,
                Boiler = 26
            };

            var temperatureReading2 = new TemperatureReading()
            {
                Id = 2,
                Timestamp = DateTime.Now.AddHours(-12),
                Basement = 31,
                FirstFloor = 32,
                SecondFloor = 33,
                Outside = 34,
                Sauna = 35,
                Boiler = 36
            };
            //ei tohiks valida neid TemperatureReading'uid mille vanus on üle 1 päeva
            var graphFilters = new GraphFilters(1,null);

            _temperatureReadingQueryMock.Setup(q => q.Get(new MatchAllSpecification<TemperatureReading>())).Returns(new[] { temperatureReading1, temperatureReading2 }.AsQueryable());

            //Act
            var result = _sut.Provide(graphFilters);

            //Assert
            Assert.That(result.Count<GraphLine>, Is.EqualTo(6));         
        }

        [Test]
        public void Provide_Returns_Only_Temperature_Values_Described_In_Filter_Symbol()
        {
            var temperatureReading1 = new TemperatureReading()
            {
                Id = 1,
                Timestamp = DateTime.Now.AddDays(-2),
                Basement = 21,
                FirstFloor = 22,
                SecondFloor = 23,
                Outside = 24,
                Sauna = 25,
                Boiler = 26
            };

            var temperatureReading2 = new TemperatureReading()
            {
                Id = 2,
                Timestamp = DateTime.Now.AddHours(-12),
                Basement = 31,
                FirstFloor = 32,
                SecondFloor = 33,
                Outside = 34,
                Sauna = 35,
                Boiler = 36
            };

            //ei tohiks valida neid TemperatureReading'uid mille vanus on üle 1 päeva
            var graphFilters = new GraphFilters(1, "Sauna,Outside");

            _temperatureReadingQueryMock.Setup(q => q.Get(new MatchAllSpecification<TemperatureReading>())).Returns(new[] { temperatureReading1, temperatureReading2 }.AsQueryable());

            //Act
            var result = _sut.Provide(graphFilters);

            //järjestan symbolite järgi et saaks kontrollida kas sisaldab ikkagi konkreetseid sümboleid.
            var orderedResult = result.OrderByDescending(q => q.Symbol).Reverse();

            //Assert
            Assert.That(result.Count<GraphLine>, Is.EqualTo(2));
            Assert.That(orderedResult.First().Symbol, Is.EqualTo("Outside"));
            Assert.That(orderedResult.Last().Symbol, Is.EqualTo("Sauna"));
            
        }
        
        [TestCaseSource(typeof(GetTemperatureReadingTestCases))]
        public bool HasValidTemperature_Filters_Out_Readings_With_Invalid_Data(TemperatureReading temperatureReading)
        {
            var minTemperatureToShow = -40;

//            configutrationMock.Setup(x => x.GetValue<int>("MinValueOnGraph")).Returns(-40);


            // Act & Assert
            return _sut.HasValidTemperature(minTemperatureToShow, temperatureReading );
        }
        
        private class GetTemperatureReadingTestCases : IEnumerable
        {
            public IEnumerator GetEnumerator()
            {
                yield return new TestCaseData(new TemperatureReading(1, DateTime.Now, -50, 2, 3, 4, 5, 6)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(2, DateTime.Now, 1, -50, 3, 4, 5, 6)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(3, DateTime.Now, 1, 2, -50, 4, 5, 6)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(4, DateTime.Now, 1, 2, 3, -50, 5, 6)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(5, DateTime.Now, 1, 2, 3, 4, -50, 6)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(6, DateTime.Now, 1, 2, 3, 4, 5, -50)).Returns(false);
                yield return new TestCaseData(new TemperatureReading(2, DateTime.Now, 1, 2, 3, 4, 5, 6)).Returns(true);
            }
        }

    }
}
