﻿using Domain.Entities;
using Domain.Specifications;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using Web.Models;

namespace Web.Tests.Models
{
    [TestFixture]
    public class HomeIndexViewModelProviderTests
    {
        private HomeIndexViewModel.Provider _sut;
        private Mock<IQuery<HumidityReading>> _humidityReadingMock;
        private Mock<IQuery<TemperatureReading>> _temperatureReadingMock;

        [Test]
        public void Provide_Returns_Correct_AdvertisingSpaceViewModel()
        {
            _temperatureReadingMock = new Mock<IQuery<TemperatureReading>>();
            _humidityReadingMock = new Mock<IQuery<HumidityReading>>();
            _sut = new HomeIndexViewModel.Provider(_temperatureReadingMock.Object, _humidityReadingMock.Object);


            var temperatureReading1 = new TemperatureReading()
            {
                Id=1,
                Timestamp = DateTime.Now.AddMinutes(-5),
                Basement = 21,
                FirstFloor = 22,
                SecondFloor = 23,
                Outside = 24,
                Sauna = 25,
                Boiler = 26
            };

            var temperatureReading2 = new TemperatureReading()
            {
                Id=2,
                Timestamp = DateTime.Now,
                Basement = 31,
                FirstFloor = 32,
                SecondFloor = 33,
                Outside = 34,
                Sauna = 35,
                Boiler = 36
            };

            var humidityReading1 = new HumidityReading()
            {
                Id = 1,
                Timestamp = DateTime.Now.AddMinutes(-5),
                Basement = 40,
                SecondFloor = 41
            };

            var humidityReading2 = new HumidityReading()
            {
                Id=2,
                Timestamp = DateTime.Now,
                Basement = 50,
                SecondFloor = 51
            };

            _temperatureReadingMock.Setup(q => q.Get(new MatchAllSpecification<TemperatureReading>())).Returns(new[] { temperatureReading1, temperatureReading2 }.AsQueryable());
            _humidityReadingMock.Setup(q => q.Get(new MatchAllSpecification<HumidityReading>())).Returns(new[] { humidityReading1, humidityReading2 }.AsQueryable());

            //Act
            var result = _sut.Provide();

            //Assert

            Assert.That(result.CurrentTimestamp, Is.InRange(DateTime.Now.AddSeconds(-3), DateTime.Now.AddSeconds(3)));
            Assert.That(result.CurrenBasementHumidity, Is.EqualTo(humidityReading2.Basement));
            Assert.That(result.CurrenSecondFloodHumidity, Is.EqualTo(humidityReading2.SecondFloor));
            Assert.That(result.CurrenFistFloorTemperature, Is.EqualTo(temperatureReading2.FirstFloor));
            Assert.That(result.CurrenHeaterTemperature, Is.EqualTo(temperatureReading2.Boiler));
            Assert.That(result.CurrenSaunaTemperature, Is.EqualTo(temperatureReading2.Sauna));
            Assert.That(result.CurrenSecondFloorTemperature, Is.EqualTo(temperatureReading2.SecondFloor));
            Assert.That(result.CurrentBasementTemperature, Is.EqualTo(temperatureReading2.Basement));
            Assert.That(result.CurrentOutsideTemperature, Is.EqualTo(temperatureReading2.Outside));
        }
    }
}
