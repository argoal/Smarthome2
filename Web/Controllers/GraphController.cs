﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class GraphController : Controller
    {
        private readonly GraphLine.IProvider _graphlineProvider;

        public GraphController(GraphLine.IProvider graphlineProvider)
        {
            _graphlineProvider = graphlineProvider;
        }

        public IActionResult Index(GraphFilters model)
        {
            return View("Index", model);
        }

        //public ActionResult LastReading()
        //{
        //    var lastTemperaturereading = _context.TemperatureReadings.Last();
        //    return Json(lastTemperaturereading);
        //}

        public ActionResult Temperature(GraphFilters filters)
        {            
            return Json(_graphlineProvider.Provide(filters));
        }       
    }
}