﻿using System;
using System.Linq;
using Domain;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeIndexViewModel.IProvider _homeIndexViewModelProvider;

        public HomeController(HomeIndexViewModel.IProvider homeIndexViewModelProvider)
        {
            _homeIndexViewModelProvider = homeIndexViewModelProvider;
        }

        // GET: Home
        public IActionResult Index()
        {
            return View(_homeIndexViewModelProvider.Provide());
        }
    }
}
