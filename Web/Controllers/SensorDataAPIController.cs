﻿using System;
using System.Threading.Tasks;
using Domain;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    //?? [Produces("application/json")]
    [Route("api/SensorData")]
    public class SensorDataApiController : Controller
    {

        private readonly DataContext _context;

        public SensorDataApiController(DataContext context)
        {
            _context = context;
        }

        // POST: api/SensorData
        [HttpPost]
        public async Task<IActionResult> PostSensorReading([FromBody] SensorData sensorData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Kas panna konstruktorisse??
            //Kui väärtus on null siis pannakse uueks väärtuseks 0

            var humidityReading = new HumidityReading()
            {               
                Timestamp = DateTime.UtcNow,
                Basement = (int)Math.Round(sensorData.BasementHumidity ?? 0),
                SecondFloor = (int)Math.Round(sensorData.SecondFloorHumidity ?? 0)
            };

            var temperatureReading = new TemperatureReading()
            {
                Timestamp = DateTime.UtcNow,                
                Basement = sensorData.BasementTemperature ?? 0,
                FirstFloor = sensorData.FirstFloorTemperature ?? 0,
                SecondFloor = sensorData.SecondFloorTemperature ?? 0,
                Outside = sensorData.OutsideTemperature ?? 0,
                Sauna = sensorData.SaunaTemperature ?? 0,
                Boiler = sensorData.HeatingTemperature ?? 0
            };

            _context.HumidityReadings.Add(humidityReading);
            _context.TemperatureReadings.Add(temperatureReading);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
