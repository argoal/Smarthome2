﻿namespace Web.Models
{
    public class GraphFilters
    {
        public int DurationDays { get; set; }
        public string Symbol { get; set; }

        public GraphFilters(int durationDays, string symbol)
        {
            DurationDays = durationDays;

            if (symbol == null)
            {
                symbol = "Basement,FirstFloor,SecondFloor,Sauna,Boiler,Outside";
            }
            Symbol = symbol;
        }

        public GraphFilters()
        {            
        }


    }
    
}
