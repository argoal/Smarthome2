﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Specifications;
using Microsoft.Extensions.Configuration;

namespace Web.Models
{
    public class GraphLine
    {
        public string Date { get; }
        public string Symbol { get; }
        public decimal Value { get; }

        public GraphLine(DateTime date, string symbol, decimal value)
        {
            Date = date.ToLocalTime().ToString("yyyy'-'MM'-'dd' 'HH':'mm");
            Symbol = symbol;
            Value = Math.Round(value,1);
        }

        public interface IProvider
        {
            List<GraphLine> Provide(GraphFilters filters);
        }

        public class Provider : IProvider
        {
            private readonly IQuery<TemperatureReading> _temperatureReadingQuery;
            private readonly IConfiguration _configuration;

            public Provider(IQuery<TemperatureReading> temperatureReadingQuery, IConfiguration configuration)
            {
                _temperatureReadingQuery = temperatureReadingQuery;
                _configuration = configuration;
            }


            //Kasutatud graafiku jaoks on vaja, et andmed oleks mapitud nii, et igas reas on ainult kuupäev, anduri sümbol ja väärtus. 
            //seega ühe sama aja kohta tehakse nii palju ridu kui on filtrites valitud andureid.
            //Hiljem saab graafikult osad read vastavalt valikule ära võtta.


            public List<GraphLine> Provide(GraphFilters filters)
            {               
                var durationDays = filters.DurationDays;

                var columnNames = filters.Symbol.Split(',');

                var minTemperatureOnGraph = _configuration.GetValue<int>("UserVariables:MinValueOnGraph");
                
                var temperatureDataWithRightDuration = _temperatureReadingQuery
                    .Get(new MatchAllSpecification<TemperatureReading>())
                    .Where(d => d.Timestamp > DateTime.Now.AddDays(-durationDays) && HasValidTemperature(minTemperatureOnGraph, d));

                var rows = SelectDataWithRightColumnNames(columnNames, temperatureDataWithRightDuration);

                return rows;
            }

            private List<GraphLine> SelectDataWithRightColumnNames(string[] columnNames, IQueryable<TemperatureReading> temperatureDataWithRightDuration)
            {
                var rows = new List<GraphLine>();

                foreach (var columnName in columnNames)
                {
                    if (columnName == "Basement")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.Basement)));
                    }

                    if (columnName == "FirstFloor")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.FirstFloor)));
                    }

                    if (columnName == "SecondFloor")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.SecondFloor)));
                    }

                    if (columnName == "Sauna")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.Sauna)));
                    }

                    if (columnName == "Boiler")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.Boiler)));
                    }

                    if (columnName == "Outside")
                    {
                        rows.AddRange(temperatureDataWithRightDuration
                            .Select(reading => new GraphLine(reading.Timestamp, columnName, reading.Outside)));
                    }
                }
                return rows;
            }

            public bool HasValidTemperature(int minTempToShow, TemperatureReading temperatureReading)
            {
                return temperatureReading.Sauna > minTempToShow
                       && temperatureReading.Boiler > minTempToShow
                       && temperatureReading.Outside > minTempToShow
                       && temperatureReading.Basement > minTempToShow
                       && temperatureReading.FirstFloor > minTempToShow
                       && temperatureReading.SecondFloor > minTempToShow;
            }
        }
    }   
}
