﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Domain.Entities;
using Domain.Specifications;

namespace Web.Models
{
    public class HomeIndexViewModel
    {
        [Display(Name = "Viimane mõõtmine: ")]
        public DateTime CurrentTimestamp { get; set; }

        [Display(Name = "Välis")]
        public decimal CurrentOutsideTemperature { get; set; }

        [Display(Name = "2. korrus")]
        public decimal CurrenSecondFloorTemperature { get; set; }

        [Display(Name = "2. korrus")]
        public int CurrenSecondFloodHumidity { get; set; }

        [Display(Name = "1. korrus")]
        public decimal CurrenFistFloorTemperature { get; set; }

        [Display(Name = "Kelder")]
        public decimal CurrentBasementTemperature { get; set; }

        [Display(Name = "Kelder")]
        public int CurrenBasementHumidity { get; set; }

        [Display(Name = "Saun")]
        public decimal CurrenSaunaTemperature { get; set; }

        [Display(Name = "Küttesüsteem")]
        public decimal CurrenHeaterTemperature { get; set; }

        public interface IProvider
        {
            HomeIndexViewModel Provide();
        }

        public class Provider : IProvider
        {
            private readonly IQuery<HumidityReading> _humidityReadingQuery;
            private readonly IQuery<TemperatureReading> _temperatureReadingQuery;

            public Provider(IQuery<TemperatureReading> temperatureReadingQuery, IQuery<HumidityReading> humidityReadingQuery)
            {
                _temperatureReadingQuery = temperatureReadingQuery;
                _humidityReadingQuery = humidityReadingQuery;
            }

            public HomeIndexViewModel Provide()
            {
                var lastTemperatureReading = _temperatureReadingQuery.Get(new MatchAllSpecification<TemperatureReading>()).OrderByDescending(t => t.Timestamp).First();

                //Need kaks näidet saavad andmebaasist kogu listi ja siis teevad siin valiku ehk ei ole head variandid
                //var lastHumidityReading = _humidityReadingQuery.Get(new MatchAllSpecification<HumidityReading>()).ToList().OrderByDescending(t => t.Timestamp).First();
                //var lastHumidityReading = _humidityReadingQuery.Get(new MatchAllSpecification<HumidityReading>()).Select(r => new { Id = r.Id, Timestamp = r.Timestamp }).OrderByDescending(t => t.Timestamp).First();

                //Siin tagastatakse andmebaasist kohe üks objekt 
                var lastHumidityReading = _humidityReadingQuery.Get(new MatchAllSpecification<HumidityReading>()).OrderByDescending(t => t.Timestamp).First();

                return new HomeIndexViewModel()
                {
                    CurrentTimestamp = lastTemperatureReading.Timestamp,
                    CurrentOutsideTemperature = Math.Round(lastTemperatureReading.Outside, 1),
                    CurrenSecondFloorTemperature = Math.Round(lastTemperatureReading.SecondFloor, 1),
                    CurrenSecondFloodHumidity = lastHumidityReading.SecondFloor,
                    CurrenFistFloorTemperature = Math.Round(lastTemperatureReading.FirstFloor, 1),
                    CurrentBasementTemperature = Math.Round(lastTemperatureReading.Basement, 1),
                    CurrenBasementHumidity = lastHumidityReading.Basement,
                    CurrenSaunaTemperature = Math.Round(lastTemperatureReading.Sauna, 1),
                    CurrenHeaterTemperature = Math.Round(lastTemperatureReading.Boiler, 1)
                };
            }
        }
    }
}
