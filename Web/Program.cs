﻿using System;
using Domain;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Web.TestData;

namespace Web
{
    public class Program
    {
        //Siia lisatud meetod andmebaasi täitmiseks test andmetega
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var isDevelopment = environment == EnvironmentName.Development;

            if (isDevelopment)
            {
                using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;
    
                    var context = services.GetRequiredService<DataContext>();
                    DbInitializer.Initialize(context);
                }
            }

            host.Run();
        }

        //See on kohe olemas
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseApplicationInsights()
                .UseStartup<Startup>()
                .Build();
    }
}
