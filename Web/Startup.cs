﻿using Domain;
using Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Models;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

//            services.AddDbContext<DataContext>(options =>
//                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            if (HostingEnvironment.IsDevelopment())
            {
                services.AddDbContext<DataContext>(options => options.UseSqlite(Configuration.GetConnectionString("SQLiteLocal")));
            }
            else
            {
                services.AddDbContext<DataContext>(options => options.UseSqlite(Configuration.GetConnectionString("SQLite")));
            }
            
            //https://docs.microsoft.com/en-us/ef/core/miscellaneous/configuring-dbcontext

            services.AddMvc();

            services.AddScoped(typeof(IQuery<>), typeof(Query<>));
            services.AddScoped<HomeIndexViewModel.IProvider, HomeIndexViewModel.Provider>();
            services.AddScoped<GraphLine.IProvider, GraphLine.Provider>();
            
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //TODO 
                app.UseExceptionHandler("/Home/Error");
            }


            //lisada käsitsi kui ei kasuta scaffolding'i
            app.UseStaticFiles();

            //lisada käsitsi kui ei kasuta scaffolding'i
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
