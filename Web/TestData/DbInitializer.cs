﻿using System;
using System.Collections.Generic;
using System.Threading;
using Domain;
using Domain.Entities;
using Microsoft.EntityFrameworkCore.Internal;

namespace Web.TestData
{
    public class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (context.TemperatureReadings.Any())
            {
                return; // DB has been seeded
            }

            var temperatureReadings = SeedTemperatureReadings();
            foreach (var t in temperatureReadings)
            {
                context.TemperatureReadings.AddAsync(t);
            }

            var humidityReadings = new[]
            {
                new HumidityReading {Id = 1, Timestamp = DateTime.Now.AddMinutes(-15), Basement = 45, SecondFloor = 55},
                new HumidityReading {Id = 2, Timestamp = DateTime.Now.AddMinutes(-10), Basement = 55, SecondFloor = 65},
                new HumidityReading {Id = 3, Timestamp = DateTime.Now.AddMinutes(-5), Basement = 34, SecondFloor = 40},
                new HumidityReading {Id = 4, Timestamp = DateTime.Now, Basement = 55, SecondFloor = 65}
            };
            foreach (var h in humidityReadings)
            {
                context.HumidityReadings.Add(h);
            }

            context.SaveChanges();
        }

        public static IEnumerable<TemperatureReading> SeedTemperatureReadings()
        {
            var temperatureReadings = new List<TemperatureReading>();

            var currentTime = DateTime.Now;

            var numberOfEntries = 1800;
            var seedDurationHours = 8760;
            var timeStepInMin = seedDurationHours * 60 / numberOfEntries;


            for (int i = 0, j=1 ; i < seedDurationHours * 60; i = i + timeStepInMin, j++)
            {
                temperatureReadings.Add(new TemperatureReading(
                    id: j,
                    timestamp: currentTime.AddMinutes(-seedDurationHours * 60 + i),
                    basement:  GetTemperatureAccordingToParameters(15, 4, 10,  seedDurationHours,  i),
                    firstFloor: GetTemperatureAccordingToParameters(20, 2, 8,  seedDurationHours,  i),
                    secondFloor: GetTemperatureAccordingToParameters(9, 5, 5,  seedDurationHours,  i),
                    sauna: GetTemperatureAccordingToParameters(50, 1, 80,  seedDurationHours,  i),
                    boiler: GetTemperatureAccordingToParameters(15, 4, 70,  seedDurationHours,  i),
                    outside: GetTemperatureAccordingToParameters(-20, 3, 12,  seedDurationHours,  i)
                ));
            }
            return temperatureReadings.ToArray();
        }

        public static decimal GetTemperatureAccordingToParameters(int horMove, int verMove, int scale, int seedDurationHours, int i)
        {
            return (decimal) (Math.Round(Math.Sin((i*verMove) * Math.PI * 2 / (seedDurationHours * 60)) * scale, 2)+horMove);
        }
    }
}